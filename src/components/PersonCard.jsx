import React, { Component } from 'react';
import Person from './Person';

export default class PersonCard extends Component {
    render() {
        return (
            <div className="card" style={ { color: 'grey', width: 150, height: 100 } }>
                <Person/>
            </div>
        );
    }
}