import React, { Component } from 'react';
import User from './User';

export default class UserCard extends Component {
    render() {
        return (
            <div className="card" style={ { color: 'grey', width: 150, height: 100 } }>
                <User/>
            </div>
        );
    }
}