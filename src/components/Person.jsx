import React, { Component } from 'react';
import MyContext from '../myContext';

export default class Person extends Component {
    render() {
        return (
            <MyContext.Consumer>
                { ({ name, setName }) => (
                    <div>
                        { name }
                        <button onClick={ () => setName('Hadi') }>Change</button> 
                    </div>
                    )
                }

            </MyContext.Consumer>
        );
    }
}