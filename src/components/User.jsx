import React, { Component } from 'react';
import MyContext from '../myContext';

export default class Person extends Component {
    static contextType = MyContext; 
    render() {
        return (
            <div>
                { this.context.name }
                <button onClick={ () => this.context.setName('Hadi') }>Change</button> 
            </div>
        );
    }
}