import React, { Component } from 'react';
import './App.css';
import PersonCard from './components/PersonCard';
import UserCard from './components/UserCard';
import MyContext from './myContext';


class App extends Component {
  state = {
      name: 'Malik',
      age: 23,
      setName: (newName) => this.setName(newName)
  }

  setName = (newName) => {
    this.setState({ ...this.state, name: newName });
  }

  render() {
    return (
      <div className="App" >
        <MyContext.Provider value={ this.state }>
          <PersonCard/>
        </MyContext.Provider>
        <MyContext.Provider value={ {name: this.state.name, setName: this.state.setName} }>
          <UserCard/>
        </MyContext.Provider>
      </div>
    );
  }
}

export default App;
